# Ponos

A mediocrely generic blog server primarily for myself, open-sourced for others.

> Ponos [...] is the personification of Hardship and Toil.

\- Wikipedia

> English: "Diarrhea"; Russian: "Ponos"

\- Google Translate

You can decide yourself what it stands for.

## Swapping Modules

You can find the module configuration in `com.vestniklas.stale.app.Main`. Available
modules can be found in `com.vestniklas.stale.services.modules`. Dependency configuration
is subject to change.

## Running the Server

Currently, the project is not exported in any usable format.

```
$ sbt
$ sbt:ponos> twirlCompileTemplates
$ sbt:ponos> compile
$ sbt:ponos> run
```

The server now runs on port 8182 on localhost. You can change that behaviour by using
the `--port <port>` / `-p <port>` CLI parameter.

---
Once the project has matured, documentation will be more exhaustive.
