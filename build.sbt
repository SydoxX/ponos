val languageVersion = "2.13.2"
val http4sVersion = "0.21.3"
val scalatestVersion = "3.1.2"

val dependencies = Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-twirl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.scalactic" %% "scalactic" % scalatestVersion,
  "org.scalatest" %% "scalatest" % scalatestVersion % "test",
  "org.scalacheck" %% "scalacheck" % "1.14.1" % "test",
  "org.rogach" %% "scallop" % "3.4.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3", // logger impl,
  "com.vladsch.flexmark" % "flexmark-all" % "0.62.2" // markdown parser
)

val compilerOptions = Seq()

lazy val root = project
  .in(file("."))
  .settings(
    name := "ponos",
    version := "0.1.0",
    scalaVersion := languageVersion,
    scalacOptions ++= compilerOptions,
    libraryDependencies ++= dependencies,
  )
  .enablePlugins(SbtTwirl)
