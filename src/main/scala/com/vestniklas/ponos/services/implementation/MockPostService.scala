package com.vestniklas.ponos.services.implementation

import java.time.LocalDateTime

import com.vestniklas.ponos.model.Post
import com.vestniklas.ponos.services.PostService

object MockPostService extends PostService {

  override def allPosts = Iterable(
    Post(
      "the-first-entry",
      LocalDateTime.of(2020, 1, 1, 10, 15),
      "The first entry",
      "# This is the entry!\nWhat a post; amazing.",
      Iterable("tag1", "tag2")
    ),
    Post(
      "another-one",
      LocalDateTime.of(2020, 1, 3, 10, 15),
      "The second entry",
      "# another magnificent entry!\nWhat a post; amazing.",
      Iterable("tag1", "tag2")
    )
  )

  override def postById(id: String) = allPosts.find(_.id == id)
}
