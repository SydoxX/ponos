package com.vestniklas.ponos.services.implementation

import com.vestniklas.ponos.model.Page
import com.vestniklas.ponos.services.PageLocatorService

object MockPageLocatorService extends PageLocatorService {

  private val mockPages = Map(
    "about" -> Page("about", "About Me", "# Hehe!\n\nThis is a bit of info about me.")
  )

  override def pageCalled(name: String) = mockPages.get(name)

}
