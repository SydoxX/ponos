package com.vestniklas.ponos.services

import com.vestniklas.ponos.model.Post

trait PostService {
  def allPosts: Iterable[Post]
  def postById(id: String): Option[Post]
}
