package com.vestniklas.ponos.services.modules

import com.vestniklas.ponos.services.{PageLocatorService, PostService}

trait PageLocatorServiceModule {
  this: ModuleBundle =>
  implicit def pageLocatorService: PageLocatorService
}

trait PostServiceModule {
  this: ModuleBundle =>
  implicit def postService: PostService
}

trait ModuleBundle
  extends PostServiceModule
    with PageLocatorServiceModule
