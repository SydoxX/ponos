package com.vestniklas.ponos.services.modules

import com.vestniklas.ponos.services.implementation.{MockPageLocatorService, MockPostService}

trait DevPageLocatorServiceModule extends PageLocatorServiceModule {
  this: ModuleBundle =>
  override implicit val pageLocatorService = MockPageLocatorService
}

trait DevPostServiceModule extends PostServiceModule {
  this: ModuleBundle =>
  override implicit val postService = MockPostService
}

