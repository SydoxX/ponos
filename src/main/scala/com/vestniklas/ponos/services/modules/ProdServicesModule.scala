package com.vestniklas.ponos.services.modules

import java.io.File

import com.vestniklas.ponos.services.implementation.LocalFilesPostService

trait ProdPageLocatorServiceModule extends PageLocatorServiceModule {
  this: ModuleBundle =>
  override implicit val pageLocatorService = ???
}

trait ProdPostServiceModule extends PostServiceModule {
  this: ModuleBundle =>

  def rootDirectory: String

  override implicit val postService =
    new LocalFilesPostService(
      postsDirectory = new File(rootDirectory + "/posts/")
    )

}
