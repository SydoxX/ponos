package com.vestniklas.ponos.services

import com.vestniklas.ponos.model.Page

trait PageLocatorService {
  def pageCalled(name: String): Option[Page]
}
