package com.vestniklas.ponos.model

import java.time.LocalDateTime

case class Post(
  id: String,
  date: LocalDateTime,
  title: String,
  content: String,
  tags: Iterable[String]
)
