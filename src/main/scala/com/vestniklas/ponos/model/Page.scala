package com.vestniklas.ponos.model

case class Page(
  name: String,
  title: String,
  content: String
)
