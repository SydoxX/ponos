package com.vestniklas.ponos.app

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import com.vestniklas.ponos.controller._
import com.vestniklas.ponos.services.modules._
import org.http4s.implicits._
import org.http4s.server.blaze._
import org.http4s.server.Router


object Main extends IOApp {

  // This is where dependencies are configured
  def moduleConfiguration(conf: PonosCli): ModuleBundle = {
    // You can swap out modules here
    new ModuleBundle
      // module: PostService
      with DevPostServiceModule
      // module: PageLocatorService
      with DevPageLocatorServiceModule

    // new ModuleBundle
    //   with DevPageLocatorServiceModule
    //   with ProdPostServiceModule {
    //     override val rootDirectory = conf.directory().stripSuffix("/")
    //   }
  }

  override def run(args: List[String]) = {
    val cliParams = new PonosCli(args)

    val dependencies = moduleConfiguration(cliParams)
    import dependencies._

    val services =
      PostsController() <+>
        DefaultController()

    BlazeServerBuilder[IO]
      .bindHttp(cliParams.port(), "localhost")
      .withHttpApp(Router("/" -> services).orNotFound)
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
  }

}
