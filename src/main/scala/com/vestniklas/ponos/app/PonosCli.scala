package com.vestniklas.ponos.app

import org.rogach.scallop.ScallopConf

class PonosCli(args: List[String]) extends ScallopConf(args) {

  val directory = opt[String](
    default = Some(System.getProperty("user.dir")),
    descr = "The root directory of the blog.",
    required = false
  )

  val port = opt[Int](
    default = Some(8182),
    descr = "The port on which to run the web server.",
    required = false
  )

  verify()

}
