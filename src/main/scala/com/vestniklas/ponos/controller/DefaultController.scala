package com.vestniklas.ponos.controller

import cats.effect.IO
import com.vestniklas.ponos.model.Page
import com.vestniklas.ponos.services.PageLocatorService
import org.http4s.dsl.impl.Root
import org.http4s.HttpRoutes
import org.http4s.dsl.io._
import org.http4s.twirl._
import play.twirl.api.Html

object DefaultController {
  def apply()(implicit pageLocatorService: PageLocatorService) =
    getPageByName

  def getPageByName(implicit pageLocatorService: PageLocatorService) =
    HttpRoutes.of[IO] {
      case GET -> Root / name =>
        pageLocatorService.pageCalled(name).fold(NotFound()) {
          case Page(_, title, content) =>
            val html = Html(content)
            Ok(partial.html.page(title)(html))
        }
    }

}
