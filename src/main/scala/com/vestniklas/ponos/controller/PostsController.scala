package com.vestniklas.ponos.controller

import cats.effect.IO
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import com.vestniklas.ponos.services.PostService
import org.http4s.dsl.impl.Root
import org.http4s.HttpRoutes
import org.http4s.dsl.io._
import org.http4s.twirl._

object PostsController extends StrictLogging {

  def apply()(implicit postService: PostService) =
    getPosts <+> getPostById

  def getPostById(implicit postService: PostService) =
    HttpRoutes.of[IO] {
      case GET -> Root / "post" / id =>
        logger.debug("Id of post to load: " + id)
        postService.postById(id) match {
          case Some(post) => Ok(html.post(post))
          case None => NotFound()
        }
    }

  def getPosts(implicit postService: PostService) =
    HttpRoutes.of[IO] {
      case GET -> Root / "posts" =>
        Ok(html.posts(postService.allPosts))
    }

}
